module ApplicationHelper
  def alerts_and_notices
    if notice
      content_tag :div, class: "row" do
        content_tag :div, class: "col-xs-12" do
          content_tag :div, class: "alert alert-success fade in" do
            content_tag :button, "×", class: "close", :"data-dismiss" => "alert"
            content_tag :i, class: "fa fa-fw fa-check"
            notice

          end
        end
      end
    end
    if alert
      content_tag :div, class: "row" do
        content_tag :div, class: "col-xs-12" do
          content_tag :div, class: "alert alert-danger fade in" do
            content_tag :button, "×", class: "close", :"data-dismiss" => "alert"
            content_tag :i, class: "fa fa-fw fa-check"
            alert

          end
        end
      end
    end
    
  end
end
