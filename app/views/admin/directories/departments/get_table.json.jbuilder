json.draw params[:draw].to_i
json.recordsTotal @total_departments.count
json.recordsFiltered @total_departments.count
json.data do
  json.array! @departments.map do |departmentsmap|
    json.name departmentsmap.name
    json.actions lambda{
      link_to("<i class='fa fa-pencil'></i>".html_safe, "#/admin/directories/departments/#{departmentsmap.id}/edit", class: "btn btn-default btn-xs") + " " +
      link_to("<i class='fa fa-times'></i>".html_safe, admin_directories_department_path(departmentsmap.id), method: :delete, class: "btn btn-danger btn-xs", "data-confirm" => "Вы уверены, что хотите удалить данную запись?")
    }.call
  end
end