json.draw params[:draw].to_i
json.recordsTotal @total_inventories.count
json.recordsFiltered @total_inventories.count
json.data do
  json.array! @inventories.map do |inventoriesmap|
    json.name inventoriesmap.number + " - " + inventoriesmap.name
    json.actions lambda{
      link_to("<i class='fa fa-pencil'></i>".html_safe, "#/admin/directories/inventories/#{inventoriesmap.id}/edit", class: "btn btn-default btn-xs") + " " +
      link_to("<i class='fa fa-times'></i>".html_safe, admin_directories_department_path(inventoriesmap.id), method: :delete, class: "btn btn-danger btn-xs", "data-confirm" => "Вы уверены, что хотите удалить данную запись?")
    }.call
  end
end