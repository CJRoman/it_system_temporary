json.draw params[:draw].to_i
json.recordsTotal @total_users.count
json.recordsFiltered @total_users.count
json.data do
  json.array! @users.map do |usersmap|
    json.email usersmap.email
    json.name usersmap.name
    json.role User::ROLES[usersmap.role]
    json.actions lambda{
      link_to("<i class='fa fa-pencil'></i>".html_safe, "#/admin/directories/users/#{usersmap.id}/edit", class: "btn btn-default btn-xs") + " " +
      link_to("<i class='fa fa-times'></i>".html_safe, admin_directories_user_path(usersmap.id), method: :delete, class: "btn btn-danger btn-xs", "data-confirm" => "Вы уверены, что хотите удалить данную запись?", remote: true)
    }.call
  end
end