json.draw params[:draw].to_i
json.recordsTotal @total_works
json.recordsFiltered @works.count

json.data do
  json.array! @works.map do |worksmap|
    json.it_department_works do
      json.created_at worksmap.created_at.strftime("%d.%m.%Y")
    end
    json.users do
      json.name worksmap.user.name
    end
    json.inventory Inventory.find_by(id: worksmap.inventory).try(:name)
    json.other_inventory worksmap.other_inventory
    json.departments do
      json.name worksmap.department.name
    end
    json.inventory_move_type worksmap.inventory_move_type
    json.detailed_client_place worksmap.detailed_client_place
    json.inventory_move_from_client_name worksmap.inventory_move_from_client_name
    json.inventory_move_from_client_telephone worksmap.inventory_move_from_client_telephone
    json.inventory_move_destination_department_id worksmap.inventory_move_destination_department_id
    json.inventory_move_destination_client_place worksmap.inventory_move_destination_client_place
    json.inventory_move_destination_client_name worksmap.inventory_move_destination_client_name
    json.inventory_move_destination_client_telephone worksmap.inventory_move_destination_client_telephone
    
    json.actions lambda{
      "#{link_to("<i class='fa fa-eye'></i>".html_safe, "#/admin/it_department/works/#{worksmap.id}", class: "btn btn-default btn-xs") if can? :read, ItDepartmentWork}" + " " +
      "#{link_to("<i class='fa fa-pencil'></i>".html_safe, "#/admin/it_department/works/#{worksmap.id}/edit", class: "btn btn-default btn-xs") if can? :edit, ItDepartmentWork}" + " " +
      "#{link_to("<i class='fa fa-times'></i>".html_safe, admin_it_department_work_path(worksmap.id), method: :delete, class: "btn btn-danger btn-xs", "data-confirm" => "Вы уверены, что хотите удалить данную запись?") if can? :destroy, ItDepartmentWork}"
    }.call
  end
end