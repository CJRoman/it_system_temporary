json.draw params[:draw].to_i
json.recordsTotal @total_works
json.recordsFiltered @works.count

json.data do
  json.array! @works.map do |worksmap|
    json.it_department_works do
      json.created_at worksmap.created_at.strftime("%d.%m.%Y")
    end
    json.projectors do
      json.name worksmap.projector.name
    end 
    json.users do
      json.name worksmap.user.name
    end
    json.projector_filter_clean worksmap.projector_filter_clean
    json.projector_lamp_replace worksmap.projector_lamp_replace
    json.projector_runtime_check worksmap.projector_runtime_check
    json.projector_other worksmap.projector_other
    json.projector_lamp_run_time worksmap.projector_lamp_run_time
    json.projector_filter_run_time worksmap.projector_filter_run_time
    
    json.actions lambda{
      "#{link_to("<i class='fa fa-eye'></i>".html_safe, "#/admin/it_department/works/#{worksmap.id}", class: "btn btn-default btn-xs") if can? :read, ItDepartmentWork}" + " " +
      "#{link_to("<i class='fa fa-pencil'></i>".html_safe, "#/admin/it_department/works/#{worksmap.id}/edit", class: "btn btn-default btn-xs") if can? :edit, ItDepartmentWork}" + " " +
      "#{link_to("<i class='fa fa-times'></i>".html_safe, admin_it_department_work_path(worksmap.id), method: :delete, class: "btn btn-danger btn-xs", "data-confirm" => "Вы уверены, что хотите удалить данную запись?") if can? :destroy, ItDepartmentWork}"
    }.call
  end
end