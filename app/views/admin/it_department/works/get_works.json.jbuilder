json.draw params[:draw].to_i
json.recordsTotal @total_works
json.recordsFiltered @filtered_works

json.data do
  json.array! @works.map do |worksmap|
    json.it_department_works do
      json.created_at worksmap.created_at.strftime("%d.%m.%Y")
    end
    json.work_description worksmap.work_description
    json.users do
      json.name worksmap.user.name
    end
    json.work_type ItDepartmentWork::WORK_TYPE[worksmap.work_type]
    json.orgtechnik worksmap.orgtechnik? ? "<i class='fa fa-check text-success'></i>" : ""
    json.departments do
      json.name worksmap.department.name
    end
    json.client_name worksmap.client_name
    json.client_telephone worksmap.client_telephone
    json.end_date worksmap.end_date.strftime("%d.%m.%Y")
    json.status ItDepartmentWork::STATUS[worksmap.status]
    json.actions lambda{
      "#{link_to("<i class='fa fa-eye'></i>".html_safe, "#/admin/it_department/works/#{worksmap.id}", class: "btn btn-default btn-xs") if can? :read, ItDepartmentWork}" + " " +
      "#{link_to("<i class='fa fa-pencil'></i>".html_safe, "#/admin/it_department/works/#{worksmap.id}/edit", class: "btn btn-default btn-xs") if can? :edit, ItDepartmentWork}" + " " +
      "#{link_to("<i class='fa fa-times'></i>".html_safe, admin_it_department_work_path(worksmap.id), method: :delete, class: "btn btn-danger btn-xs", "data-confirm" => "Вы уверены, что хотите удалить данную запись?") if can? :destroy, ItDepartmentWork}"
    }.call
  end
end