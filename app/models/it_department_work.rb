class ItDepartmentWork < ActiveRecord::Base
	WORK_TYPE = ["Очень срочное (в течение дня)", "Срочное (в течение недели)", "Среднесрочное (в течение двух недель)", "Долгосрочное (более двух недель)", "Администратор"]
	STATUS = ["Запланировано", "Невыполнимо", "Выполнено"]
	INV_MOVE_TYPE = ["Официальное перемещение", "Временное перемещение", "Ремонт/техобслуживание", "Списание"]
	PROJECTOR_WORK_TYPES = ["Чистка фильтра", "Замена лампы", "Проверка наработки"]
	belongs_to :user
	belongs_to :department
	belongs_to :projector
	has_many :it_department_work_comments
	paginates_per 50

	validates :inventory_move_type, :from_department_id, :detailed_from_client_place, :inventory_move_from_client_name, :inventory_move_destination_department_id, :inventory_move_destination_client_place, :inventory_move_destination_client_name, presence: true, if: "inventory_move == 1 || inventory_move == 2"
	validates :other_inventory, presence: true, if: "(inventory_move == 1 || inventory_move == 2) and inventory.blank?"

	validates :projector_id, :projector_lamp_run_time, :projector_filter_run_time, presence: true, if: "projector_job?"

	validates :work_type, :end_date, :department_id, :detailed_client_place, :client_name, :status, presence: true, if: "inventory_move == 0 || inventory_move == nil"
	validates :work_description, presence: true, if: "projector_job == 0"

end
