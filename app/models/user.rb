class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :it_department_works

  ROLES = {"user" => "Пользователь", "admin" => "Администратор", "oit" => "IT Отдел, РИС"}

  validates :name, presence: true
end
