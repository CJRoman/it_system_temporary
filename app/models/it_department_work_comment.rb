class ItDepartmentWorkComment < ActiveRecord::Base
	belongs_to :it_department_work

	validates_presence_of :it_department_work_id, :comment
end
