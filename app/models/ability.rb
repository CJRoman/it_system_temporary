class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new 
    
    if user.role == "admin"
      can :manage, :all
    end  

    if user.role == "oit"
      can :manage, ItDepartmentWork if user.boss?
      can [:create, :read, :update, :get_works, :get_inventory_movings, :get_projector_jobs, :create_comment, :get_select_data], ItDepartmentWork, :user_id => user.id
      can :change, :it_department_works_users if user.boss?
    end

  end
end
