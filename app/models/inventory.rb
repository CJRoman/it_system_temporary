class Inventory < ActiveRecord::Base
	validates :name, :number, presence: true

	def select_name_and_number
		"#{name} - #{number}"
	end
end
