class Admin::ItDepartment::WorksController < ApplicationController
  layout false
  before_action :set_work, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource class: "ItDepartmentWork"

  def index
  end

  def show
  end

  def get_works
    #Загрузка доступных записей
    @works = ItDepartmentWork.joins(:user, :department).accessible_by(current_ability).where("inventory_move = 0 OR inventory_move = 2").where(projector_job: false).all
    #Удалить стобец "Действия"
    params["columns"].delete("10")
    #Фильтрация по всем столбцам
    params["columns"].each_with_index{|c, i| @works = @works.where("#{c[1]["data"]} LIKE ?", "%#{c[1]["search"]["value"]}%")}
    #Фильтрация по полю "Поиск"
    #@works = @works.where("it_department_works.created_at LIKE ? OR work_description LIKE ? OR users.name LIKE ? OR work_type LIKE ? OR orgtechnik LIKE ? OR departments.name LIKE ? OR client_name LIKE ? OR client_telephone LIKE ? OR end_date LIKE ? or status LIKE ?", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%")
    #Сортировка по столбцу
    params["columns"]["0"]["data"] = "created_at"
    @filtered_works = @works.count
    @works = @works.offset(params[:start]).limit(params[:length]).order(params["columns"][params["order"]["0"]["column"]]["data"] => params["order"]["0"]["dir"].to_sym)

    #Выборка максимума записей без фильтров
    @total_works = ItDepartmentWork.joins(:user, :department).accessible_by(current_ability).where("inventory_move = 0 OR inventory_move = 2").where(projector_job: false).count
  end

  def get_inventory_movings
    #Загрузка доступных записей
    @works = ItDepartmentWork.joins(:user, :department).accessible_by(current_ability).where("inventory_move = 1 OR inventory_move = 2").all
    #Удалить стобец "Действия"
    params["columns"].delete("12")
    #Фильтрация по всем столбцам
    params["columns"].each_with_index{|c, i| @works = @works.where("#{c[1]["data"]} LIKE ?", "%#{c[1]["search"]["value"]}%")}
    #Фильтрация по полю "Поиск"
    #@works = @works.where("it_department_works.created_at LIKE ? OR work_description LIKE ? OR users.name LIKE ? OR work_type LIKE ? OR orgtechnik LIKE ? OR departments.name LIKE ? OR client_name LIKE ? OR client_telephone LIKE ? OR end_date LIKE ? or status LIKE ?", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%")
    #Сортировка по столбцу
    params["columns"]["0"]["data"] = "created_at"
    @works = @works.offset(params[:start]).limit(params[:length]).order(params["columns"][params["order"]["0"]["column"]]["data"] => params["order"]["0"]["dir"].to_sym)
    #Выборка максимума записей без фильтров
    @total_works = ItDepartmentWork.joins(:user, :department).accessible_by(current_ability).where("inventory_move = 1 OR inventory_move = 2").count
  end

  def get_projector_jobs
    #Загрузка доступных записей
    @works = ItDepartmentWork.joins(:user, :department, :projector).accessible_by(current_ability).where(projector_job: true).all
    #Удалить стобец "Действия"
    params["columns"].delete("9")
    #Фильтрация по всем столбцам
    params["columns"].each_with_index{|c, i| @works = @works.where("#{c[1]["data"]} LIKE ?", "%#{c[1]["search"]["value"]}%")}
    #Фильтрация по полю "Поиск"
    #@works = @works.where("it_department_works.created_at LIKE ? OR work_description LIKE ? OR users.name LIKE ? OR work_type LIKE ? OR orgtechnik LIKE ? OR departments.name LIKE ? OR client_name LIKE ? OR client_telephone LIKE ? OR end_date LIKE ? or status LIKE ?", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%", "%#{params["search"]["value"]}%")
    #Сортировка по столбцу
    params["columns"]["0"]["data"] = "created_at"
    @works = @works.offset(params[:start]).limit(params[:length]).order(params["columns"][params["order"]["0"]["column"]]["data"] => params["order"]["0"]["dir"].to_sym)
    #Выборка максимума записей без фильтров
    @total_works = ItDepartmentWork.joins(:user, :department, :projector).accessible_by(current_ability).where(projector_job: true).count
  end

  def create_comment
    @work = ItDepartmentWork.find(params[:work_id])
    @comment = ItDepartmentWorkComment.create(it_department_work_id: @work.id, comment: params[:comment])
    respond_to do |format|
      if @comment.save
        format.js { render js: "window.location.reload()", notice: "Комментарий добавлен"}
      else
        format.js { render js: "alert('Не удалось добавить комментарий')" }
      end
    end
  end

  def new
    @work = ItDepartmentWork.new
  end

  def create
    @work = ItDepartmentWork.new(work_params)
    @work.user_id ||= current_user.id
    respond_to do |format|
      if @work.save
        format.js { render js: "window.location = '/admin#/admin/it_department/works'", notice: "Работа добавлена"}
      else
        format.js { render "failed", alert: "Не удалось создать запись" }
      end
    end
  end

  def edit
  end

  def update
    @work.user_id ||= current_user.id
    respond_to do |format|
      if @work.update(work_params)
        format.js { render js: "window.location = '/admin#/admin/it_department/works'", notice: "Работа обновлена"}
      else
        format.js { render "failed", alert: "Не удалось создать запись" }
      end
    end
  end

  def destroy
    @work.destroy
    respond_to do |format|
      format.html { redirect_to "/admin#/admin/it_department/works", notice: 'Запись удалена' }
    end
  end

  def get_select_data
    case params[:select_element]
    when "projector"
      results = Projector.where("name LIKE ? OR location LIKE ?", "%#{params[:q]}%", "%#{params[:q]}%").limit(25).select(:id).select("CONCAT(name, ' - ', location) as text")
    when "inventory"
      results = Inventory.where("name LIKE ? OR number LIKE ? or client_name LIKE ? or place LIKE ?", "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%").limit(25).select(:id).select("CONCAT(IFNULL(number, ''), ' - ', IFNULL(name, ''), ', ', IFNULL(place, ''), ', ', IFNULL(client_name, '')) as text")
    when "user"
      results = User.where("name LIKE ?", "%#{params[:q]}%").limit(25).select(:id).select("name as text")
    when "department"
      results = Department.where("name LIKE ?", "%#{params[:q]}%").limit(25).select(:id).select("name as text")
    end
    render json: {more: false, results: results} 
  end

  private

  def set_work
    @work = ItDepartmentWork.find(params[:id])
  end

  def work_params
    params.require(:it_department_work).permit(:work_type, :user_id, :work_description, :orgtechnik, :inventory_move, :end_date, :department_id, :client_name, :client_telephone, :status, :inventory, :other_inventory, :from_department_id, :detailed_from_client_place, :inventory_move_from_client_name, :inventory_move_from_client_telephone, :detailed_client_place, :inventory_move_type, :projector_job, :inventory_move_destination_department_id, :inventory_move_destination_client_place, :inventory_move_destination_client_name, :inventory_move_destination_client_telephone, :projector_id, :projector_work_type, :projector_lamp_run_time, :projector_filter_run_time, :projector_filter_clean, :projector_lamp_replace, :projector_runtime_check, :projector_other, :projector_other_description, :orgtechnik_inventory, :orgtechnik_toner_replace, :orgtechnik_print_replace, :orgtechnik_other, :orgtechnik_other_description)
  end
end
