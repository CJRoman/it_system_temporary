class Admin::AdminController < ApplicationController
  layout "admin", only: :index
  before_action :authenticate_user!

  def index
  end

  def dashboard
  	it_department_users = User.where(role: ["oit", "admin"]).all
  	@planned_works_array = []
  	@unable_works_array = []
  	@completed_works_array = []
  	it_department_users.each do |user|
  		@planned_works_array << {name: user.name, data: [[1, user.it_department_works.where("it_department_works.status = 0").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 6.months).utc.beginning_of_month, (Time.now - 5.months).utc.end_of_month).count], [2, user.it_department_works.where("it_department_works.status = 0").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 5.months).utc.beginning_of_month, (Time.now - 4.months).utc.end_of_month).count], [3, user.it_department_works.where("it_department_works.status = 0").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 4.months).utc.beginning_of_month, (Time.now - 3.months).utc.end_of_month).count], [4, user.it_department_works.where("it_department_works.status = 0").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 3.months).utc.beginning_of_month, (Time.now - 2.months).utc.end_of_month).count], [5, user.it_department_works.where("it_department_works.status = 0").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 2.months).utc.beginning_of_month, (Time.now - 1.months).utc.end_of_month).count], [6, user.it_department_works.where("it_department_works.status = 0").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 1.months).utc.beginning_of_month, (Time.now)).count]]}
  		@unable_works_array << {name: user.name, data: [[1, user.it_department_works.where("it_department_works.status = 1").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 6.months).utc.beginning_of_month, (Time.now - 5.months).utc.end_of_month).count], [2, user.it_department_works.where("it_department_works.status = 1").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 5.months).utc.beginning_of_month, (Time.now - 4.months).utc.end_of_month).count], [3, user.it_department_works.where("it_department_works.status = 1").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 4.months).utc.beginning_of_month, (Time.now - 3.months).utc.end_of_month).count], [4, user.it_department_works.where("it_department_works.status = 1").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 3.months).utc.beginning_of_month, (Time.now - 2.months).utc.end_of_month).count], [5, user.it_department_works.where("it_department_works.status = 1").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 2.months).utc.beginning_of_month, (Time.now - 1.months).utc.end_of_month).count], [6, user.it_department_works.where("it_department_works.status = 1").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 1.months).utc.beginning_of_month, (Time.now)).count]]}
  		@completed_works_array << {name: user.name, data: [[1, user.it_department_works.where("it_department_works.status = 2").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 6.months).utc.beginning_of_month, (Time.now - 5.months).utc.end_of_month).count], [2, user.it_department_works.where("it_department_works.status = 2").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 5.months).utc.beginning_of_month, (Time.now - 4.months).utc.end_of_month).count], [3, user.it_department_works.where("it_department_works.status = 2").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 4.months).utc.beginning_of_month, (Time.now - 3.months).utc.end_of_month).count], [4, user.it_department_works.where("it_department_works.status = 2").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 3.months).utc.beginning_of_month, (Time.now - 2.months).utc.end_of_month).count], [5, user.it_department_works.where("it_department_works.status = 2").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 2.months).utc.beginning_of_month, (Time.now - 1.months).utc.end_of_month).count], [6, user.it_department_works.where("it_department_works.status = 2").where("it_department_works.created_at BETWEEN ? AND ?", (Time.now - 1.months).utc.beginning_of_month, (Time.now)).count]]}
  	end
  end

  def import
    render nothing: true

  end

end
