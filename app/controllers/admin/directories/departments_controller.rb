class Admin::Directories::DepartmentsController < ApplicationController
  layout false
  before_action :set_department, only: [:edit, :update, :destroy]
  load_and_authorize_resource
  def index
  end

  def get_table
    @total_departments = Department.all
    @departments = Department.all
    @departments = @departments.where("name LIKE LOWER(?)",  "%"+params["columns"]["0"]["search"]["value"].downcase+"%") if !params["columns"]["0"]["search"]["value"].to_s.blank?
    @departments = @departments.where("name LIKE ?", "%"+params["search"]["value"].to_s+"%")

    case params["order"]["0"]["column"]
    when "0"
      orderable = "name"
    when "1"
      orderable = "name"
    end

    @departments = @departments.page(params[:start].to_i + 1).per(params[:length]).order(orderable => params["order"]["0"]["dir"] == "asc" ? :asc : :desc)
  end

  def new
    @department = Department.new
  end

  def create
    @department = Department.new(department_params)

    respond_to do |format|
      if @department.save
        format.html { redirect_to "/admin#/admin/directories/departments", notice: 'Новое подразделение создано' }
        format.json { render json: @department, status: :created, location: @department }
      else
        format.html { redirect_to "/admin#/admin/directories/departments/new", alert: "Не удалось создать запись" }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to "/admin#/admin/directories/departments", notice: 'Подразделение отредактировано' }
        format.json { render json: @department, status: :ok, location: @department }
      else
        format.html { redirect_to "/admin#/admin/directories/departments/#{@department.id}/edit" }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @department.destroy
    respond_to do |format|
      format.html { redirect_to "/admin#/admin/directories/departments", notice: 'Запись удалена' }
      format.json { head :no_content }
    end
  end

  private

  def set_department
    @department = Department.find(params[:id])
  end

  def department_params
    params.require(:department).permit(:name)
  end
end
