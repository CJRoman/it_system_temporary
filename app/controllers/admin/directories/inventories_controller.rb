class Admin::Directories::InventoriesController < ApplicationController
  layout false
  before_action :set_inventory, only: [:edit, :update, :destroy]
  load_and_authorize_resource

  def index
  end

  def get_table
    @total_inventories = Inventory.all
    @inventories = Inventory.all
    @inventories = @inventories.where("name LIKE LOWER(?) OR number LIKE LOWER(?)",  "%"+params["columns"]["0"]["search"]["value"].downcase+"%", "%"+params["columns"]["0"]["search"]["value"].downcase+"%") if !params["columns"]["0"]["search"]["value"].to_s.blank?
    @inventories = @inventories.where("name LIKE ? OR number LIKE ?", "%"+params["search"]["value"].to_s+"%", "%"+params["search"]["value"].to_s+"%")

    case params["order"]["0"]["column"]
    when "0"
      orderable = "name"
    when "1"
      orderable = "number"
    end

    @inventories = @inventories.page(params[:start].to_i + 1).per(params[:length]).order(orderable => params["order"]["0"]["dir"] == "asc" ? :asc : :desc)
  end

  def new
    @inventory = Inventory.new
  end

  def create
    @inventory = Inventory.new(inventory_params)

    respond_to do |format|
      if @inventory.save
        format.html { redirect_to "/admin#/admin/directories/inventories", notice: 'МТЦ создана' }
        format.json { render json: @inventory, status: :created, location: @inventories }
      else
        format.html { redirect_to "/admin#/admin/directories/inventories/new", alert: "Не удалось создать запись" }
        format.json { render json: @inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @inventory.update(inventory_params)
        format.html { redirect_to "/admin#/admin/directories/inventories", notice: 'МТЦ отредактирована' }
        format.json { render json: @inventory, status: :ok, location: @inventories }
      else
        format.html { redirect_to "/admin#/admin/directories/inventories/#{@inventory.id}/edit" }
        format.json { render json: @inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @inventory.destroy
    respond_to do |format|
      format.html { redirect_to "/admin#/admin/directories/inventories", notice: 'Запись удалена' }
      format.json { head :no_content }
    end
  end

  private

  def set_inventory
    @inventory = Inventory.find(params[:id])
  end

  def inventory_params
    params.require(:inventory).permit(:name, :number)
  end
end
