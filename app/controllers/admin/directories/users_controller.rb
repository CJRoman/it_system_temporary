class Admin::Directories::UsersController < ApplicationController
  layout false
  before_action :set_user, only: [:edit, :update, :destroy]
  load_and_authorize_resource

  def index
  end

  def get_table
    @total_users = User.all
    @users = User.all
    @users = @users.where("email LIKE LOWER(?)",  "%"+params["columns"]["0"]["search"]["value"].downcase+"%") if !params["columns"]["0"]["search"]["value"].to_s.blank?
    @users = @users.where("name LIKE LOWER(?)",  "%"+params["columns"]["1"]["search"]["value"].downcase+"%") if !params["columns"]["1"]["search"]["value"].to_s.blank?
    @users = @users.where("role LIKE LOWER(?)",  "%"+params["columns"]["2"]["search"]["value"].downcase+"%") if !params["columns"]["2"]["search"]["value"].to_s.blank?
    @users = @users.where("email LIKE ? OR name LIKE ? OR role LIKE ?", "%"+params["search"]["value"].to_s+"%", "%"+params["search"]["value"].to_s+"%", "%"+params["search"]["value"].to_s+"%")

    case params["order"]["0"]["column"]
    when "0"
      orderable = "email"
    when "1"
      orderable = "name"
    end

    @users = @users.page(params[:start].to_i + 1).per(params[:length]).order(orderable => params["order"]["0"]["dir"] == "asc" ? :asc : :desc)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
     @user.skip_confirmation!

    respond_to do |format|
      if @user.save
        format.html { redirect_to "/admin#/admin/directories/users", notice: 'Пользователь создан' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.js { render "create" }
      end
    end
  end

  def edit
  end

  def update
    @user.skip_reconfirmation!
    if params[:user][:password].blank?
      respond_to do |format|
        if @user.update_without_password(user_params)
          format.html { redirect_to "/admin#/admin/directories/users", notice: 'Пользователь отредактирован' }
        else
          format.js { render "update" }
        end
      end
    else
      respond_to do |format|
        if @user.update_attributes(user_params)
          format.html { redirect_to "/admin#/admin/directories/users", notice: 'Пользователь отредактирован' }
        else
          format.js { render "update" }
        end
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to "/admin#/admin/directories/users", notice: 'Запись удалена' }
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :boss, :role)
  end
end
