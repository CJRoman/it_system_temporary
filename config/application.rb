require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module Tgmu
  class Application < Rails::Application
    config.time_zone = 'Asia/Vladivostok'
    config.i18n.default_locale = :ru
  end
end
