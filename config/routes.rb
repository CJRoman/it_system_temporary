Rails.application.routes.draw do
  root to: "admin/admin#index"
  devise_for :users
  
  namespace :admin do
    root to: "admin#index"
    get "/" => "admin#index", as: :index
    get "dashboard" => "admin#dashboard", as: :dashboard
  	namespace :directories do
  	  resources :departments
  	  resources :inventories
      resources :users
      post "departments/get_table"
      post "inventories/get_table"
      post "users/get_table"
  	end
    namespace :it_department do
      resources :works
      post "works/get_works/" => "works#get_works", response_to: :json
      post "works/get_inventory_movings/" => "works#get_inventory_movings", response_to: :json
      post "works/get_projector_jobs/" => "works#get_projector_jobs", response_to: :json
      post "works/create_comment" => "works#create_comment", response_to: :json
      get "works/get_select_data/:select_element" => "works#get_select_data"
    end
  end
  get "admin/admin/import"
end