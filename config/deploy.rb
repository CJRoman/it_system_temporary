set :domain, '151.248.120.166'
set :user, 'railsuser'
set :application, "it_system_temporary"

require "bundler/capistrano"
require "rvm/capistrano"

set :rvm_ruby_string, '2.0.0'

set :repository,  "https://CJRoman:OPtY61Ll@bitbucket.org/CJRoman/it_system_temporary.git"
set :deploy_to, "/home/railsuser/it_system_temporary"

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, domain                          # Your HTTP server, Apache/etc
role :app, domain                          # This may be the same as your `Web` server
role :db,  domain, :primary => true # This is where Rails migrations will run

set :deploy_via, :remote_cache
set :scm, 'git'
set :branch, 'master'
set :scm_verbose, true
set :use_sudo, false
set :rails_env, :production
set :auth_methods, :password
ssh_options[:forward_agent] = true
default_run_options[:pty] = true
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "my_local")]

namespace :deploy do
	desc "cause Passenger to restart"
	task :restart do
		run "touch #{current_path}/tmp/restart.txt"
	end
end

after "deploy:update_code", :bundle_install
desc "Install the neccessary prerequesties"
task :bundle_install, :roles => :app do
	run "cd #{release_path} && bundle install"
end