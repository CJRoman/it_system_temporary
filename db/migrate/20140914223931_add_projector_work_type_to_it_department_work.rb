class AddProjectorWorkTypeToItDepartmentWork < ActiveRecord::Migration
  def change
    add_column :it_department_works, :projector_work_type, :integer
  end
end
