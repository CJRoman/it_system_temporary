class CorrectData < ActiveRecord::Migration
  def migrate(direction)
  	ItDepartmentWork.all.each do |w|
  		w.work_description = "0" if w.work_description == nil
  		w.end_date = Time.now if w.end_date == nil
  		w.client_name = "0" if w.client_name == nil
  		w.client_telephone = "0" if w.client_telephone == nil
  		w.inventory = "0" if w.inventory == nil
  		w.detailed_client_place = "0" if w.detailed_client_place == nil
  		w.inventory_move_from_client_name = "0" if w.inventory_move_from_client_name == nil
  		w.inventory_move_from_client_telephone = "0" if w.inventory_move_from_client_telephone == nil
  		w.detailed_from_client_place = "0" if w.detailed_from_client_place == nil
  		w.from_department_id = "0" if w.from_department_id == nil
  		w.other_inventory = "0" if w.other_inventory == nil
  		w.inventory_move_type = "0" if w.inventory_move_type == nil
  		w.projector_job = false if w.projector_job == nil
  		w.projector_filter_run_time = "0" if w.projector_filter_run_time == nil
  		w.projector_lamp_run_time = "0" if w.projector_lamp_run_time == nil
  		w.projector_work_type = "0" if w.projector_work_type == nil
  		w.inventory_move_destination_department_id = "0" if w.inventory_move_destination_department_id == nil
  		w.inventory_move_destination_client_place = "0" if w.inventory_move_destination_client_place == nil
  		w.inventory_move_destination_client_name = "0" if w.inventory_move_destination_client_name == nil
  		w.inventory_move_destination_client_telephone = "0" if w.inventory_move_destination_client_telephone == nil
  		w.save
  	end
  end
end
