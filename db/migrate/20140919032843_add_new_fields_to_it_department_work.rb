class AddNewFieldsToItDepartmentWork < ActiveRecord::Migration
  def change
    add_column :it_department_works, :orgtechnik_inventory, :string
    add_column :it_department_works, :orgtechnik_toner_replace, :boolean
    add_column :it_department_works, :orgtechnik_print_replace, :boolean
    add_column :it_department_works, :orgtechnik_other, :boolean
    add_column :it_department_works, :orgtechnik_other_description, :string
  end
end
