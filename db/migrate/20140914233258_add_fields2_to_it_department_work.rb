class AddFields2ToItDepartmentWork < ActiveRecord::Migration
  def change
    add_column :it_department_works, :inventory_move_destination_department_id, :integer
    add_column :it_department_works, :inventory_move_destination_client_place, :string
    add_column :it_department_works, :inventory_move_destination_client_name, :string
    add_column :it_department_works, :inventory_move_destination_client_telephone, :string

    ItDepartmentWork.all.each do |w|
    	if w.inventory_move?
          w.inventory_move_destination_department_id = w.detailed_client_place
    	  w.inventory_move_destination_client_name = w.client_name
    	  w.inventory_move_destination_client_telephone = w.client_telephone
    	  w.inventory_move_destination_client_place = w.detailed_client_place
    	  w.detailed_client_place = nil
    	  w.client_name = nil
    	  w.client_telephone = nil
    	  w.client_telephone = nil
    	  w.save
    	end
    end
  end
end
