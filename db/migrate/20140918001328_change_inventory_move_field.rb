class ChangeInventoryMoveField < ActiveRecord::Migration
  def change
  	change_column :it_department_works, :inventory_move, :integer
  end
end
