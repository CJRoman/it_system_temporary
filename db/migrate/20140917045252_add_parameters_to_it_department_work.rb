class AddParametersToItDepartmentWork < ActiveRecord::Migration
  def change
    add_column :it_department_works, :projector_filter_clean, :boolean
    add_column :it_department_works, :projector_lamp_replace, :boolean
    add_column :it_department_works, :projector_runtime_check, :boolean
    add_column :it_department_works, :projector_other, :boolean
    add_column :it_department_works, :projector_other_description, :string
  end
end
