class CreateProjectorLamps < ActiveRecord::Migration
  def change
    create_table :projector_lamps do |t|
      t.string :oem
      t.string :model
      t.integer :lamp_count

      t.timestamps
    end
  end
end
