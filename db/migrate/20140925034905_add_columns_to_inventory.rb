class AddColumnsToInventory < ActiveRecord::Migration
  def change
    add_column :inventories, :place, :string
    add_column :inventories, :client_name, :string
  end
end
