class AddFieldsToItDepartmentWorks < ActiveRecord::Migration
  def change
    add_column :it_department_works, :from_department_id, :integer
    add_column :it_department_works, :other_inventory, :string
  end
end
