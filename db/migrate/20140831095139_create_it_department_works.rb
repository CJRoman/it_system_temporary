class CreateItDepartmentWorks < ActiveRecord::Migration
  def change
    create_table :it_department_works do |t|
      t.integer :work_type
      t.integer :user_id
      t.text :work_description
      t.datetime :end_date
      t.boolean :orgtechnik
      t.integer :department_id
      t.string :client_name
      t.string :client_telephone
      t.integer :status

      t.timestamps
    end
  end
end
