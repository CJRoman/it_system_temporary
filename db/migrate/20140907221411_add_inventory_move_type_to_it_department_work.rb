class AddInventoryMoveTypeToItDepartmentWork < ActiveRecord::Migration
  def change
    add_column :it_department_works, :inventory_move_type, :string
  end
end
