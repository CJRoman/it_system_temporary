class CreateProjectors < ActiveRecord::Migration
  def change
    create_table :projectors do |t|
      t.string :name
      t.boolean :stationary
      t.integer :lamp_id
      t.string :location

      t.timestamps
    end
  end
end
