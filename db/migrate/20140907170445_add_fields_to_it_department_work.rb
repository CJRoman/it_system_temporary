class AddFieldsToItDepartmentWork < ActiveRecord::Migration
  def change
    add_column :it_department_works, :detailed_client_place, :string
    add_column :it_department_works, :inventory_move_from_client_name, :string
    add_column :it_department_works, :inventory_move_from_client_telephone, :string
    add_column :it_department_works, :detailed_from_client_place, :string
  end
end
