class CreateItDepartmentWorkComments < ActiveRecord::Migration
  def change
    create_table :it_department_work_comments do |t|
      t.integer :it_department_work_id
      t.text :comment

      t.timestamps
    end
  end
end
