class AddProjectorJobToItDepartmentWork < ActiveRecord::Migration
  def change
    add_column :it_department_works, :projector_job, :boolean
    add_column :it_department_works, :projector_id, :integer
    add_column :it_department_works, :projector_filter_run_time, :string
    add_column :it_department_works, :projector_lamp_run_time, :string
  end
end
