# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140925034905) do

  create_table "departments", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inventories", force: true do |t|
    t.string   "name"
    t.string   "number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "place"
    t.string   "client_name"
  end

  create_table "it_department_work_comments", force: true do |t|
    t.integer  "it_department_work_id"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "it_department_works", force: true do |t|
    t.integer  "work_type"
    t.integer  "user_id"
    t.text     "work_description"
    t.datetime "end_date"
    t.boolean  "orgtechnik"
    t.integer  "department_id"
    t.string   "client_name"
    t.string   "client_telephone"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "inventory"
    t.integer  "inventory_move"
    t.string   "detailed_client_place"
    t.string   "inventory_move_from_client_name"
    t.string   "inventory_move_from_client_telephone"
    t.string   "detailed_from_client_place"
    t.integer  "from_department_id"
    t.string   "other_inventory"
    t.string   "inventory_move_type"
    t.boolean  "projector_job"
    t.integer  "projector_id"
    t.string   "projector_filter_run_time"
    t.string   "projector_lamp_run_time"
    t.integer  "projector_work_type"
    t.integer  "inventory_move_destination_department_id"
    t.string   "inventory_move_destination_client_place"
    t.string   "inventory_move_destination_client_name"
    t.string   "inventory_move_destination_client_telephone"
    t.boolean  "projector_filter_clean"
    t.boolean  "projector_lamp_replace"
    t.boolean  "projector_runtime_check"
    t.boolean  "projector_other"
    t.string   "projector_other_description"
    t.string   "orgtechnik_inventory"
    t.boolean  "orgtechnik_toner_replace"
    t.boolean  "orgtechnik_print_replace"
    t.boolean  "orgtechnik_other"
    t.string   "orgtechnik_other_description"
  end

  create_table "projector_lamps", force: true do |t|
    t.string   "oem"
    t.string   "model"
    t.integer  "lamp_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projectors", force: true do |t|
    t.string   "name"
    t.boolean  "stationary"
    t.integer  "lamp_id"
    t.string   "location"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "name"
    t.string   "role",                   default: "user", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "boss"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
